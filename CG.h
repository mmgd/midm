#ifndef H_CG_H
#define H_CG_H


//#include "bitmapTest.c"
#include "roaring.h"
#include "lista.h"
#include "listaseq.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libpq-fe.h>
#include <math.h>

#define valor 50


#define MAXSTR 2500
#define Nome 100
#define Header 470
#define DEBUG 0
#define TAMREG 255
#define TESTEBANCO 1
int pontos=0;


typedef struct registro{
    int tam; //tamanho do registro
    int tamTotal;//tamanho total do regitro
    int tamNome;
    int id; //identificador do registro
    int numPonto; //tamanho do vetor de pontos
    int tipo; //alerta, engarrafamento ou irrregularidade
    char nome[Nome]; //nome da rua do registro
    char tempoDia[Nome]; //tempo de ocorrencia do evento
    char tempoHora[Nome];
    double x[valor]; //ccoordenada do registro
    double y[valor]; //ccoordenada do registro
    double numX; //cooordenada da CG
    double numY; //cooordenada da CG
}registro;
typedef struct registro* registroP; //ponteiro para registro

typedef struct Block{
    int tamGravado; //tamanho dos registros gravados
    int idBloco; //identificador do bloco
    double menorTempo; //menor tempo dos registros do bloco
    double maiorTempo; //maior tempo dos regitros do bloco
    int idCG; //identificador da CG correspondente
    int numReg; //numero de registros no bloco
    registroP * registros;
}Block;
typedef struct Block *bloco; //ponteiro para bloco

typedef struct cg{
    bloco blocoCG;
}cg;
typedef struct cg *CG; //ponteiro para CG


typedef struct cgsBloco{
    int idCgs; //id da CG
    lista listaBlocos; //lista ligada com id dos blocos
}cgsBloco;
typedef struct cgsBloco *vetorBloco; //ponteiro para cgsBloco


typedef struct grad{
    double deslocamentoX; //para numeros negativos
    double deslocamentoY; //para numeros negativos
    double x1; //primeiro ponto de interesse
    double y1; //primeiro ponto de interesse
    double x2; //ultimo ponto de interesse
    double y2; //ultimo ponto de interesse
    double passoX; //tamanho do X em cada celula
    double passoY; //tamanho do Y em cada celula
    int tamBytes; //tamanho do bloco
    int l; //numero de linhas
    int c; //numero de colunas
    int existebitmap0;
    int existebitmap1;
    int existebitmap2;
    long int periodoDia;
    long int periodoHora; //tempo de inicio do periodo atual
    long periodo; //periodo de tempo para mudanca de arquivo, indices bitmap e vetor de bloco
    CG tipo0; //apontador para a CG - alerta
    CG tipo1; //apontador para a CG - engarrafamento
    CG tipo2; //apontador para a CG - irregularidades
    int numBlocos0; //numero de blocos - alerta
    int numBlocos1; //numero de blocos - engarrafamento
    int numBlocos2; //numero de blocos - irregularidades
    FILE *arq0; //apontador para arquivo - alerta
    FILE *arq1; //apontador para arquivo - engarrafamento
    FILE *arq2; //apontador para arquivo - irregularidades
    char filename0[Nome];
    char filename1[Nome];
    char filename2[Nome];
    vetorBloco v0; //apontador para o vetor de bloco - alerta
    vetorBloco v1; //apontador para o vetor de bloco - engarrafamento
    vetorBloco v2; //apontador para o vetor de bloco - irregularidades
    roaring_bitmap_t *btipo0; //apontador para o bitmap - alerta
    roaring_bitmap_t *btipo1; //apontador para o bitmap - engarrafamento
    roaring_bitmap_t *btipo2; //apontador para o bitmap - irregularidades


}grad;
typedef struct grad *gradeP;



gradeP IniciaPrograma(double *, double *, double *, double *, int *, int *);
void FinalizaPeriodo(gradeP);
void FinalizaPrograma(gradeP grade);
void leituraCoordenadas(double *, double *, double *, double *);
void TamanhoMatriz(int *, int *);
gradeP TamanhoXeY(double, double, double, double, int, int);
CG criaCGs(gradeP);
vetorBloco criavetor(gradeP );
void iniciaCgs(gradeP, int);
int calculoVetor(int, int, int);
void calculaPeriodoBloco(bloco );
void gravaRegistro(CG, FILE *, gradeP, int);
void imprimeVetor(vetorBloco, int , gradeP);
void gravaRegistrosFimLeitura(gradeP,int,PGconn *conn);
void insereBlocoVetor(vetorBloco, long int, int);
void arrumaIDbloco(gradeP, int, bloco);
void arrumaEndbloco(gradeP , int , bloco, int , long int );
void insereNoCG(registroP , CG , gradeP, FILE *,PGconn *conn);
void imprimeBloco(bloco, int);
int alocacaoPonto(int,char [], char[], gradeP, double, double, char[],int, PGconn *conn);
int verificaX(gradeP, double );
int verificaY(gradeP , double );
int verificaCG(registroP, double, double, gradeP, char *);
double calculaDistancia(double, double, double, double);
registroP inserePontoIntermediario(registroP,gradeP, double, double);
int verificaPontos(registroP,gradeP,int, double, double,PGconn *conn);
void leituraAlerta(registroP, gradeP, char line[MAXSTR],int,PGconn *conn);
int leituraEngarrafamento(registroP, gradeP, char line[MAXSTR],int,PGconn *conn);
int leituraIrregularidade(registroP, gradeP, char line[MAXSTR],int,PGconn *conn);
void imprimeBitmap(roaring_bitmap_t *, int, gradeP);
void RuaCGs(gradeP,registroP, roaring_bitmap_t *, int);
void lerPeriodo(char *, registroP);
int achaMenor(long int,long int,long int ,int , int , int, int, int, int);
void regsTab(CG c,int tipo,gradeP g,PGconn *conn);
void TabelaRegistros(gradeP g,bloco b, int tipo,PGconn *conn);
void criaTabelaRegistros(gradeP grade,PGconn *conn);
void criaBancoGCs(gradeP ,int,PGconn *conn);
void armazenaTabBitmap(gradeP,PGconn *);
void armazenaBanco(vetorBloco, int,int, gradeP,PGconn *);
void deletaBancoGCs(gradeP,int, PGconn *conn);
void criaBancoInicio(gradeP,PGconn *conn);
void criaTabCGsFixa(gradeP, PGconn *conn);
char * lerBitmap(FILE *);

//-------------------------------------------------------------------------------------------
// 4bytes para o end bloco, 4 id bloco, 2 bytes qtde, 2  id Cg , end registros dentro do arquivo 



#endif
