#ifndef H_LISTASEQ_H
#define H_LISTASEQ_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STRNOME 300
#define MAXSTR2 30


typedef struct reg {
  int CG;
  int tamNome;
  char nome[STRNOME];
  double x;
  double y;
  struct reg *proximo;
}reg; 

typedef reg *Reg;
//---------------------------------------------------------------------------
// listaSeqSeq encadeada
typedef struct listaseq {

  unsigned int tamanho;
  Reg primeiro;
}listaseq;

typedef listaseq *listaSeq;
//---------------------------------------------------------------------------



unsigned int tamanhoListaSeq(listaSeq l);
int listaVazia(listaSeq l);
Reg primeiroNoSeq(listaSeq l);
Reg proximoNolistaSeq(Reg n);
char * conteudolistaSeq(Reg n);
int imprimeLista (listaSeq l);
void inserelistaSeq( Reg, listaSeq l);
listaSeq constroilistaSeq(void);
int destroilistaSeq(listaSeq l);
int removeNolistaSeq(listaSeq, Reg);
void buscaRegistro(FILE *fp,long int endereco,listaSeq l, int CG,int cont);
int buscaRegistro2(FILE *fp,long int endereco,double x10,double y10,double x20,double y20,int cont);
int contido(double x, double y, double x10, double y10, double x20, double y20);
int intersect(double x[],double y[],double xlista,double ylista,int qtde);
void buscaArquivo(FILE *fp, long int endereco,listaSeq l);
int buscaArquivo2(FILE *fp, long int endereco, int contido,double x10,double y10,double x20,double y20);
int regIgual(int tam, char * nomeRz, int qtaxy, double x[], double y[],Reg n);
int NaotemIgual(listaSeq l,int tam,char * nomeRz,double x,double y);

#endif

