
#include "lista.h"

//---------------------------------------------------------------------------
// devolve o número de nós da lista l

unsigned int tamanhoLista(lista l) { return l->tamanho; }



//---------------------------------------------------------------------------
// devolve o primeiro nó da lista l,
//      ou NULL, se l é vazia

no primeiroNo(lista l) { return l->primeiro; }



//---------------------------------------------------------------------------
// devolve o conteúdo do nó n
//      ou NULL se n = NULL

long int conteudo(no n) { return n->endereco; }



//---------------------------------------------------------------------------
// devolve o sucessor do nó n,
//      ou NULL, se n for o último nó da lista

no proximoNo(no n) { return n->proximo; }



//---------------------------------------------------------------------------
// cria uma lista vazia e a devolve
//
// devolve NULL em caso de falha

lista constroiLista(void) {

  lista l = malloc(sizeof(struct lista));

  if ( ! l )
    return NULL;

  l->primeiro = NULL;
  l->tamanho = 0;

  return l;
}


//---------------------------------------------------------------------------
// desaloca a lista l e todos os seus nós
//
// se destroi != NULL invoca
//
//     destroi(conteudo(n))
//
// para cada nó n da lista.
//
// devolve 1 em caso de sucesso,
//      ou 0 em caso de falha

int destroiLista(lista l) {

  no p;
  int ok=1;
  while ( (p = primeiroNo(l)) ) {

    l->primeiro = proximoNo(p);

    free(p);
  }

  free(l);

  return ok;
}


//---------------------------------------------------------------------------
// insere um novo nó na lista l cujo conteúdo é p
//
// devolve o no recém-criado
//      ou NULL em caso de fa

void insereLista(long int end, lista l) {

  no n;
  no novo = malloc(sizeof(struct no));

  if (tamanhoLista(l)==0){
    l->primeiro=novo;
  
  }
  else  {
    for(n=primeiroNo(l); proximoNo(n)!=NULL; n=proximoNo(n));
    n->proximo=novo;
  }

  novo->endereco = end;
  novo->proximo = NULL;
  ++l->tamanho;
  //printf("tamanho lista %d ++++++\n ",l->tamanho);

}



int removeNo(struct lista *l, struct no *rno) {
  int r = 1;
  if (l->primeiro == rno) {
    l->primeiro = rno->proximo;
    free(rno);
    l->tamanho--;
    return r;
  }
  for (no n = primeiroNo(l); n->proximo; n = proximoNo(n)) {
    if (n->proximo == rno) {
      n->proximo = rno->proximo;

      free(rno);
      l->tamanho--;
      return r;
    }
  }
  return 0;
}
