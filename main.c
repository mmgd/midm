
#include "CG.c"
#include "Banco.c"
#include "CG.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[ ]){
    //area of interest
    double x1,y1,x2,y2, passoX, passoY;
    int l,c,tipo, limiteBloco,i, cont, per=0,lincol;
    gradeP grade;
    int opt;
    char tempo[20]; 
    PGconn *conn = PQconnectdb("user=mariana dbname=postgis_test");


    
    grade=IniciaPrograma(&x1,&x2,&y1,&y2,&l,&c);
    printf("Tamanho do Periodo - dia e hora\n");
    scanf("%s\n",tempo);

    time_t ano,t2,t3;
	long int calculo;	
    lincol=l*c;

	sscanf(tempo, "%ld %ld %ld", &ano,&t2,&t3);
	t2*=-1;
	t3*=-1;
	grade->periodoDia=(ano*365)+(t2*30)+t3;
    scanf("%ld\n",&grade->periodoHora);

    printf("Dia %ld, hora %ld\n",grade->periodoDia,grade->periodoHora);

    scanf("%ld",&grade->periodo);
    printf("PERIODO %ld\n",grade->periodo);
 
    criaBancoInicio(grade,conn);

    loopPrincipal(grade,0,conn); 

   // printf("finaliza\n");
    FinalizaPrograma(grade);
       if ((opt=getopt(argc, argv, "rtns"))!=-1){
        switch (opt){
            case 'r':deletaBancoGCs(grade,0,conn);break;
            case 't':comparacaoNaN();exit(0) ;break;
            case 'n':consulta2();exit(0) ;break;
            case 's':criaTabCGsFixa(grade,conn);break;
        }    
    }

    printf("\nfinaliza Leitura\n");
    PQfinish(conn);
    printf("fim\n");
}

