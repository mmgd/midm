# MIDET
In the urban environment, data collected from traffic events can become elements of study to help plan cities and metropolises. Although much data has already been stored, the challenge is transforming this set of Spatio-temporal data into mobility knowledge to assist in this planning. Traffic events, such as traffic jams and alerts, are continuously produced using apps like Waze. Due to the speed with which this data is reported, applications generally store events as individual records. Even though this storage model can guarantee a low insertion cost in a database, it produces low performance in Spatial-temporal queries. To address this problem, this dissertation proposes a geningographic partitioning of the area of interest in a matrix form, creating a tessellation composed of juxtaposed Geographic Cells (CGs). Events that occurred in the same CG are then stored in a grouped manner to optimize their recovery. We have tested the creation of a M}ethod for Indexation of Ttraffic Events (MIDET). MIDET adopts mixed storage, with files composed of groups of events that occurred in the same CG and a relational base for indexing data based on bitmaps. MIDET's motivation is indexing historical traffic events, which occurred in a given geographic region, related to a period, intending to obtain good performance for Spatial-temporal queries on such records. As traffic events are reported on large volumes of data, an additional objective is that the method should have a low cost of \textit{insertion}. The method has application in several contexts. The adopted study case was the basis of the application Waze (https://github.com/joinvalle/Joinville-Smart-Mobility) that contains events from Joinville city. Spatial-temporal queries performed with MIDET showed superior performance compared to storage in the relational database clustered by CGs. The tests performed demonstrated that mixed storage and the use of bitmaps proposed by MIDET are advantageous.


//-----------
To compile: make
remove object: make clean
//----------- Files -----------
CG.h
CG.c 
lista.c
lista.h
Banco.h
seq.c
listagem.h
main.c
roaring.c
roaring.h
makefile
teste.txt - input parameters (area of interest, # of lines and # of columns for CG - geographic cells, starting date, time interval) 
//----------- Structures Main Program-----------
registro
	size, id, number of points, type, vectors of poits x and y, coordinate (x,y) for related CG.

bloco
	size, id, id of related CG, number of registros.

CG
	Cg vector that consists of pointer to a block.

vetorBloco
	vector of cgs bloco, with for each position: id for related CG, a linked list of correlated blocs.

grade
	auxiliar structure with interest area (x1,y1,x2,y2), size of x and y for cells, # of rows and collumns, pointer to CGs structure for each type of registro, # of blocks for each type of registro, pointer to a file for each type of registro, pointer to a vetor de bloco for each type of registro.

Tabbitmaps
	temporal structure stored in DB: time interval (beginning /end),  and bitmap for all data types (jams, alerts, irregularities), file names for each data type. 

//----------- Structures file-----------
Record files are separate for each data type (jams, alerts, irregularities) and each time interval

File names: type name + time interval count (Eg jam0, irregularities1)

block header in file
	block address, block ID#, IDCG #, NumReg # and address for all registers.

//----------- Usage -----------
Options 
-r remove all tables from DB
-s create tables () in DB
-t first query “Which streets had traffic jams and warnings intersecting in the first week of 2018?” 
-n second query “ How many traffic jams occurred in an area of interest in October 2017?” 



//--------------------------------------------
The origin base Waze is stored in Postgres and was obtained from the project Smart Mobility organized by the city of Joinville-SC https://github.com/joinvalle/Joinville-Smart-Mobility. The granting of data for the referred project was provided to the State University of Santa Catarina (UDESC), partner of this work. Origin base has 13 Gigabytes (GB), containing data from September 2017 to September 2018. From this base, Waze base was created, with only attributes involved in experiments' query. An R tree was also built on the Geometry attribute.


The implementation was conducted on a machine running Mac OS 10.15.2 on Dual-Core Intel Core m3 with 1.1 GHz and 8 GB of main memory.