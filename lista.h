#ifndef H_LISTA_H
#define H_LISTA_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
//#include "CG.h"


typedef struct no {
  long int endereco;

  struct no *proximo;
}tipoNo; 

typedef tipoNo *no;
//---------------------------------------------------------------------------
// lista encadeada
typedef struct lista {

  unsigned int tamanho;
  no primeiro;
}tipoLista;

typedef tipoLista *lista;
//---------------------------------------------------------------------------



//------------------------------------------------------------------------------
// devolve o número de nós da lista l

unsigned int tamanhoLista(lista l);


//------------------------------------------------------------------------------
// devolve o primeiro nó da lista l,
//      ou NULL, se l é vazia

no primeiroNo(lista l);

//------------------------------------------------------------------------------
// devolve o sucessor do nó n,
//      ou NULL, se n for o último nó da lista

no proximoNo(no n);

//------------------------------------------------------------------------------
// devolve o conteúdo do nó n
//      ou NULL se n = NULL 

long int conteudo(no n);

//------------------------------------------------------------------------------
// insere um novo nó na lista l cujo conteúdo é p
//
// devolve o no recém-criado 
//      ou NULL em caso de falha

void insereLista(long int end, lista l);

//------------------------------------------------------------------------------
// cria uma lista vazia e a devolve
//
// devolve NULL em caso de falha

lista constroiLista(void);


//------------------------------------------------------------------------------
// desaloca a lista l e todos os seus nós
// 
// se destroi != NULL invoca
//
//     destroi(conteudo(n)) 
//
// para cada nó n da lista. 
//
// devolve 1 em caso de sucesso,
//      ou 0 em caso de falha

int destroiLista(lista l);


int removeNo(struct lista *l, struct no *rno);
#endif

