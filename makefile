CC=gcc
CFLAGS= -lpq
DEPS =  CG.h listaseq.h lista.h roaring.h 
OBJ = roaring.o seq.o lista.o main.o 

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

main: $(OBJ)
	gcc -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm main *.o
