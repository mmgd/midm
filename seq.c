
#include "listaseq.h"


//---------------------------------------------------------------------------
// devolve o número de nós da lista l

unsigned int tamanhoListaSeq(listaSeq l) { return l->tamanho; }

int listaVazia(listaSeq l) { 
  if (tamanhoListaSeq(l)==0)
    return 1;
  else return 0;
}

//---------------------------------------------------------------------------
// devolve o primeiro nó da lista l,
//      ou NULL, se l é vazia

Reg primeiroNoSeq(listaSeq l) { return l->primeiro; }



//---------------------------------------------------------------------------
// devolve o conteúdo do nó n
//      ou NULL se n = NULL

char * conteudolistaSeq(Reg n){ return n->nome; }



//---------------------------------------------------------------------------
// devolve o sucessor do nó n,
//      ou NULL, se n for o último nó da lista

Reg proximoNolistaSeq(Reg n) { return n->proximo; }



//---------------------------------------------------------------------------
// cria uma lista vazia e a devolve
//
// devolve NULL em caso de falha

listaSeq constroilistaSeq(void) {

  listaSeq l = malloc(sizeof(listaseq));

  if ( ! l )
    return NULL;

  l->primeiro = NULL;
  l->tamanho = 0;

  return l;
}


//---------------------------------------------------------------------------
// desaloca a lista l e todos os seus nós
//
// se destroi != NULL invoca
//
//     destroi(conteudo(n))
//
// para cada nó n da lista.
//
// devolve 1 em caso de sucesso,
//      ou 0 em caso de falha

int destroilistaSeq(listaSeq l){

  Reg p;
  int ok=1;
  while ( (p = primeiroNoSeq(l)) ) {

    l->primeiro = proximoNolistaSeq(p);

    free(p);
  }

  free(l);

  return ok;
}

int imprimeLista(listaSeq l){
  Reg n;
  if (listaVazia(l)==1){
    return 0;
  }

  for(n=primeiroNoSeq(l); proximoNolistaSeq(n)!=NULL; n=proximoNolistaSeq(n)){
    printf("C %d, %s %lf %lf\n",n->CG,n->nome,n->x,n->y);
  }
  printf("fim imprime lista\n");
  return 1;
}


//---------------------------------------------------------------------------
// insere um novo nó na lista l cujo conteúdo é p
//
// devolve o no recém-criado
//      ou NULL em caso de fa

void inserelistaSeq(Reg r, listaSeq l) {
Reg n;

  if (tamanhoListaSeq(l)==0){
    l->primeiro=r;
  
  }
  else  {
    for(n=primeiroNoSeq(l); proximoNolistaSeq(n)!=NULL; n=proximoNolistaSeq(n));
    n->proximo=r;
  }

  r->proximo = NULL;
  ++l->tamanho;
}



int removeNolistaSeq(listaSeq l, Reg rno) {
  int r = 1;
  if (l->primeiro == rno) {
    l->primeiro = rno->proximo;
    free(rno);
    l->tamanho--;
    return r;
  }
  for ( Reg n = primeiroNoSeq(l); n->proximo; n = proximoNolistaSeq(n)) {
    if (n->proximo == rno) {
      n->proximo = rno->proximo;

      free(rno);
      l->tamanho--;
      return r;
    }
  }
  return 0;
}

void buscaRegistro(FILE *fp,long int endereco,listaSeq l, int CG, int cont){
  //le cada registro do bloco e insere na lista
  int tam, tipo,qtaxy,i,off=0;
	char hora[MAXSTR2],dia[MAXSTR2],separa[5];
	double x,y;

    
  if (cont == 0 )
    fseek(fp,endereco,SEEK_SET);

  fscanf(fp,"%d",&tam);
 
  //printf("tam %d\n",tam);

  char *nomeRz=malloc(sizeof(char)*tam);
  separa[0]=getc(fp);

  for (i=0; i < tam ; ++i){
    nomeRz[i]=getc(fp);
    //printf("i %d NOME %s\n",i,nomeRz);
  }    
  nomeRz[tam]='\0';
  //printf("tam %d\n",tam);
 // printf("NOME %s\n",nomeRz);

	fscanf(fp,"%d %s %s %d",&tipo,hora,dia,&qtaxy);

  //como é alerta so existe um x e um y
  fscanf(fp,"%s %lf %s %lf",separa,&x,separa,&y);

  if (NaotemIgual(l,tam,nomeRz,x,y)==1){
    Reg r = malloc(sizeof(reg));
    r->CG=CG;
    r->tamNome=tam;
    strcpy(r->nome,nomeRz);
    r->x=x;
    r->y=y;
    inserelistaSeq(r,l);
  }  
  //printf("fim do busca registro\n");
  free(nomeRz);
}


int buscaRegistro2(FILE *fp,long int endereco,double x10,double y10,double x20,double y20, int cont){
	int tam, tipo,qtaxy,i,off=0;
	char hora[MAXSTR2],dia[MAXSTR2],separa[5];
	double x,y;

    //so é necessario posicionar no comeco do primeiro registro do bloco
    //if (cont == 0 )
      fseek(fp,endereco,SEEK_SET);
      
    fscanf(fp,"%d",&tam);
    //printf("tam %d\n",abs(tam));

    char nomeRz[abs(tam)];
    separa[0]=getc(fp);

    for (i=0; i < tam ; i++){
        nomeRz[i]=getc(fp);
        //printf("i %d NOME %s\n",i,nomeRz);
    }    
    nomeRz[tam]='\0';
    //printf("%d NOME %s",tam,nomeRz);
 
	fscanf(fp,"%d %s %s %d",&tipo,hora,dia,&qtaxy);
    //printf(" %d %s %s %d ",tipo,hora,dia,qtaxy);

    for (i=0; i < qtaxy; i++){
        fscanf(fp,"%s %lf %s %lf",separa,&x,separa,&y);
        //printf(" %lf %lf ",x,y);
         //if intersect
        if (contido(x,y,x10,y10,x20,y20)) 
            return 1; 
    }   
   // puts(" ");
    return 0;
}


int contido(double x, double y, double x10, double y10, double x20, double y20){
    if ((x >= x10)&&(x <= x20)&&(y >= y10)&&(y <= y20))
        return 1;
return 0;
}

int intersect(double x[],double y[],double xlista,double ylista, int qtde){
  int i;
  for (i=0; i < qtde; ++i){
    if ((x[i] == xlista)&&(y[i] == ylista)){
      //printf("%lf == %lf,%lf == %lf\n",x[i],xlista,y[i],ylista);
      return 1;
    }  
  }
return 0;
}


void buscaArquivo(FILE *fp, long int endereco, listaSeq l){
	//encontra o endereco do bloco para comparacao

  int id,cgid,qtda,i;
  long int end;
    
	fseek(fp,endereco,SEEK_SET);
    
  fscanf(fp,"%ld %d %d %d",&end,&id,&cgid,&qtda);
  //printf("%d %d %d \n",id,cgid,qtda);

  int v[qtda];
  for (i=0; i< qtda; i++)
    fscanf(fp,"%d", &v[i]);
  for (i=0; i< qtda; i++){
   //printf("Busca registro end %d\n",v[i]);
  //le cada registro do bloco e insere na lista
    buscaRegistro(fp,v[i],l,cgid,i);
  }   
  //printf("fim do busca arquivo\n");
}


int buscaArquivo2(FILE *fp, long int endereco, int contido,double x10,double y10,double x20,double y20){
	//encontra o endereco do bloco para comparacao

  int id,cgid,qtda,i,registros=0;
  long int end;
    
	fseek(fp,endereco,SEEK_SET);
    
  fscanf(fp,"%ld %d %d %d",&end,&id,&cgid,&qtda);
  //printf("%d %d %d \n",id,cgid,qtda);

  if (contido) return qtda;

  else {
    int v[qtda];
    for (i=0; i< qtda; i++)
      fscanf(fp,"%d", &v[i]);
    for (i=0; i< qtda; i++){
      //printf("Busca registro end %d cont %d\n",v[i],i);
      registros+=buscaRegistro2(fp,v[i],x10,y10,x20,y20,i);
    }    	
  }  
    return registros;  
}


int regIgual(int tam, char * nomeRz, int qtaxy, double x[], double y[],Reg n){
  if (tam == n->tamNome){
		if(strcmp(nomeRz,n->nome)==0){
			//printf("IGUAL %s %s \n",nomeRz,n->nome);
			if (intersect(x,y,n->x,n->y,qtaxy)==1){
        return 1;
      }
    }
  }
  return 0;   
}

int NaotemIgual(listaSeq l,int tam,char * nomeRz,double x,double y){
  Reg n;
  //listra vazia
  if (listaVazia(l)==1){
    return 1;
  }  

  for(n=primeiroNoSeq(l); proximoNolistaSeq(n)!=NULL; n=proximoNolistaSeq(n)){
    if (tam == n->tamNome){
		  if(strcmp(nomeRz,n->nome)==0){
			  if ((x == n->x)&&(y <= n->y)){
          //printf("IGUAL %s %s \n",nomeRz,n->nome);
          return 0;
        }
      }
    }
  }
  return 1;
}