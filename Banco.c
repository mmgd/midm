
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <libpq-fe.h>
#include <inttypes.h>
#include <time.h>
#include "CG.h"


#define BUG 1
#define DEBUG2 0
int cont=0;
int res=0;


void comparacaoNaN();
int blocoComBloco(FILE *fp0,long int endbloco0,FILE *fp1,long int endbloco1);
int consulta2();
int comparaRegistros(PGresult* res1, int ntuples1, listaSeq sequencia, int k, int cg0);
listaSeq criaEstrutura(char * nomearquivo,long int endereco, int ntuples1);
void AcrescentaEstrutura(char * nomearquivo, int endereco,listaSeq sequencia);
void blocoComSequencia(FILE *fp1,long int endbloco1,listaSeq sequencia);
void RegComSequencia(FILE *fp1,long int endreg,listaSeq sequencia, int cont);



//2 bytes por 2 enderecos de registro

/* void teste(){
	clock_t start, end;
    double cpu_time_used;
	FILE *teste;
	int i=36;
	teste=fopen("Alerta0","r");
	end = clock();
   	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("\n========================\nCPU Time parte arquivo %lf para %d \n",cpu_time_used,i);


	printf("chamando funcao para o arquivo inteiro\n");
	arquivoInteiro(teste);
	printf("Comecando arquivo 2 vez\n");
	fseek(teste,0,SEEK_SET);
	arquivoInteiro(teste);

	fclose(teste);
} */

void loopPrincipal(gradeP g, int num,PGconn *conn){
    int tipo, troca=0;
    char tempo[MAXSTR], tempoDia[MAXSTR], tempoHora[MAXSTR];
    double x1,y1;
    int primeiraVez=1;
    char nome[MAXSTR], line[MAXSTR*MAXSTR];
    long int calculo;

    int fimArquivo0=0,fimArquivo1=0,fimArquivo2=0;
    int mudouI=1,mudouJ=1,mudouK=1;
    int i=0,j=0,k=0;
    int pubt0,pubt1,pubt2;

    int  ntuples0,ntuples1, ntuples2;
    //PGconn *conn = PQconnectdb("user=mariana dbname=postgis_test");
    static PGresult* res0 = NULL;
    static PGresult* res1 = NULL;
    static PGresult* res2 = NULL;
    int ncols0,ncols1,ncols2;
    char *linep=malloc(MAXSTR*sizeof(char));

  if (PQstatus(conn) == CONNECTION_BAD) {
      
      fprintf(stderr, "Connection to database failed: %s\n",
          PQerrorMessage(conn));
      
  }

  //----------------------------------------------------------------------------
  //leitura alerta
    res0 = PQexec(conn, "SELECT street, pub_utc_date, location FROM alertssimpl");

    char *street0, *pub0,*location0;
    int street_col0 = PQfnumber(res0,"street");
    int pub_col0 = PQfnumber(res0,"pub_utc_date");
    int location_col0 = PQfnumber(res0,"location");
    if (street_col0 <0 || pub_col0 < 0  || location_col0 < 0){
        printf("Alerta resultado invalido\n");
    }
    ntuples0=PQntuples(res0);
      printf("Alerta %d tuples retun\n", ntuples0);

    //leitura engarrafamento
    res1 = PQexec(conn, "SELECT street, pub_utc_date, line FROM jamssimpl");

    char  *street1, *pub1, *line1;
    int street_col1 = PQfnumber(res1,"street");
    int pub_col1 = PQfnumber(res1,"pub_utc_date");
    int line_col1 = PQfnumber(res1,"line");

    if (street_col1 < 0 || pub_col1 < 0 || line_col1 < 0){
        printf("Eng resultado invalido\n");
    }
    ntuples1=PQntuples(res1);
    printf("Eng %d tuples retun\n", ntuples1);

    //leitura Irregularidades
    res2 = PQexec(conn, "SELECT street, update_utc_date, line FROM irregularitiessimpl");

    char *street2, *up2, *line2;
    int street_col2 = PQfnumber(res2,"street");
    int up_col2 = PQfnumber(res2,"update_utc_date");
    int line_col2 = PQfnumber(res2,"line");
    
    if ( street_col2 <0 || up_col2 < 0 || line_col2 < 0){
        printf("IRR resultado invalido\n");
    }

    ntuples2=PQntuples(res2);
      printf("IRR %d tuples retun\n", ntuples2);

    //----------------------------------------------------------------------------

    time_t ano,t2,t3,hora;
    long int calc0,calc1,calc2;

    iniciaCgs(g,num);
    int usadoI=0, usadoJ=0, usadoK=0,usado=0;

    while ((!fimArquivo0) || (!fimArquivo1) || (!fimArquivo2)){

        //------------------------------------------------------------------
        //parte alerta
        if((mudouI == 1) && (i < ntuples0)){
            
            street0 = PQgetvalue(res0,i, street_col0);
            pub0 = PQgetvalue(res0,i,pub_col0);
            location0 = PQgetvalue(res0,i,location_col0);

            //printf("TIPO 0 street %s pub %s road %s \n", street0,pub0, road0);
            sscanf(pub0, "%ld %ld %ld %ld", &ano,&t2,&t3,&hora);
            t2*=-1;
            t3*=-1;
            calc0=(ano*365)+(t2*30)+t3;
            i++;
            mudouI=0;
            usadoI=0;
        }
        //estragando o alerta para parar de escolher ele
        else if (usadoI){
            calc0=10000000000000;
            mudouI=-1;
            fimArquivo0=1;
        }
        //parte engarrafamento
        if ((mudouJ == 1) && (j < ntuples1)){
            
            street1 = PQgetvalue(res1,j, street_col1);
            pub1 = PQgetvalue(res1,j,pub_col1);
            line1 = PQgetvalue(res1,j,line_col1);

            pubt1=atoi(pub1);
            //printf("TIPO 1street %s pub %s road %s \n",street1,pub1, road1);
            sscanf(pub1, "%ld %ld %ld %ld", &ano,&t2,&t3,&hora);
            t2*=-1;
            t3*=-1;
            calc1=(ano*365)+(t2*30)+t3;
            j++;
            mudouJ=0;
            usadoJ=0;
        }
        else if(usadoJ){
            calc1=10000000000000;
            mudouJ=-1;
            fimArquivo1=1;
        }
        //parte irregularidade
        if ((mudouK == 1) && (k < ntuples2)){
        
            street2 = PQgetvalue(res2,k, street_col2);
            up2 = PQgetvalue(res2,k,up_col2);
            line2 = PQgetvalue(res2,k,line_col2);

            pubt2=atoi(up2);
            sscanf(up2, "%ld %ld %ld %ld", &ano,&t2,&t3,&hora);
            t2*=-1;
            t3*=-1;
            calc2=(ano*365)+(t2*30)+t3;
            k++;
            mudouK=0;
            usadoK=0;
        }
        else if(usadoK){
            calc2=10000000000000;
            mudouK=-1;
            fimArquivo2=1;
        }

        if (!(fimArquivo0 && fimArquivo1 && fimArquivo2)){

            switch(achaMenor(calc0,calc1,calc2,fimArquivo0,fimArquivo1,fimArquivo2, usadoI, usadoJ,usadoK)){
                case 0: mudouI=1;
                        usadoI=usado=1;
                        calc0=1000000000;
                        strcpy(nome,street0);
                        //printf("Nome rua %s \n", nome);
                        //printf("retornou 0\n");
                        tipo=0;
                        strcpy(tempo,pub0);
                        strcpy(line,location0); break;
                case 1: mudouJ=1;
                        usadoJ=usado=1;
                        calc1=1000000000;
                        strcpy(nome,street1);
                        ///printf("Nome rua %s \n", nome);
                        //printf("retornou 1\n");
                        tipo=1;
                        strcpy(tempo,pub1);
                        strcpy(line, line1); break;
                case 2: mudouK=1;
                        usadoK=usado=1;
                        calc2=1000000000;
                        strcpy(nome,street2);
                        //printf("Nome rua %s \n", nome);
                        //printf("retornou 2\n");
                        tipo=2;
                        strcpy(tempo,up2);
                        strcpy(line, line2); break;
                default : printf("Entrou no default+++++++++++++++++++\n");break;
            }

            
            //------------------------------------------------------------------
            time_t ano,t2,t3,hora;

            sscanf(tempo, "%ld %ld %ld %ld", &ano,&t2,&t3,&hora);
            t2*=-1;
            t3*=-1;
            calculo=(ano*365)+(t2*30)+t3;
            //printf("\n \n LINE ORIGINAL %s \n \n \n ",line);

            char separa1[MAXSTR], sub[MAXSTR], separa2[MAXSTR],separa3[MAXSTR],separa4[MAXSTR], separa5[MAXSTR];

            sscanf(line,"%s %lf %s %s %lf %s",separa1,&x1,separa2, separa3,&y1,separa4);
            //printf("ESCOLHIDO TIPO %d NOME %s TEMPO %s X1 %lf Y1 %lf usado %d\n",tipo,nome,tempo,x1,y1, usado);

            sprintf(sub,"%s %lf%s %s %lf%s",separa1,x1,separa2, separa3,y1,separa4);

            int lenSub=strlen(sub);
            
            //se muda de periodo
            if ((calculo - g->periodoDia) > (g->periodo)){
                printf("Mudou de periodo\n");
                ++num;
                troca=1;
                //caso o periodo tenha terminado antes da qtde de registros          
                gravaRegistrosFimLeitura(g,num,conn);

                FinalizaPeriodo(g);
                iniciaCgs(g,num);
                alocacaoPonto(tipo,nome,tempo,g,x1,y1,line,lenSub,conn);
            
                //printf("FIM PERIODO\n");
                
            }
            else {
                troca=0;
                alocacaoPonto(tipo,nome,tempo,g,x1,y1,line,lenSub,conn);
            }
        }
        else printf("FIM DE TODOS OS ARQUIVOS\n========\n");

    }
    //printf("saiu Arquivos\n fimArquivos %d %d %d\n -----------------\n",fimArquivo0,fimArquivo1,fimArquivo2);
    if (troca == 0 ) gravaRegistrosFimLeitura(g,num,conn);
    FinalizaPeriodo(g);
    printf("Finaliza periodo \n");
    //PQfinish(conn);
    free(linep);
}


void comparacaoNaN(){
	int cg, ntuples0, ntuples1,i,k=0;
	PGconn *conn = PQconnectdb("user=mariana dbname=postgis_test");
  static PGresult* res0 = NULL;
	static PGresult* res1 = NULL;
  listaSeq sequencia;
  int cgAnt=-1;

	//consulta 1 parte alerta 
  //por arquivo tambem
	res0 = PQexec(conn,"with r (intbit, arqa, arqe) as (select roaringbitmap(i.engarrafamento) & roaringbitmap(i.alerta), i.arquivoalerta, i.arquivoengarrafamento from tabbitmaps4 i where periododiafim > 736572) select k.cg_id, arqa, blocoa from r, vetor_bloco_alerta4 k where rb_contains(r.intbit,k.cg_id) order By cg_id,arqa;");
	char *cg_id0,*arquivoalerta,*endbloco0;
	int cgId0= PQfnumber(res0, "cg_id");
	int arquivo0 = PQfnumber(res0, "arqa");
	int kbloco0= PQfnumber(res0, "blocoa");	

  //consulta 1 parte engarrafamento
  res1 = PQexec(conn,"with r (intbit, arqa, arqe) as (select roaringbitmap(i.engarrafamento) & roaringbitmap(i.alerta), i.arquivoalerta, i.arquivoengarrafamento from tabbitmaps4 i where periododiafim > 736572) select j.cg_id, arqe, blocoe from r, vetor_bloco_engarrafamento4 j where rb_contains(r.intbit,j.cg_id) order By cg_id,arqe;");

	ntuples0=PQntuples(res0);
  ntuples1=PQntuples(res1);

  printf("%d tuples retun\n", ntuples0);
  printf("%d tuples retun\n", ntuples1); 
	
	for(i=0; i < ntuples0; ++i){

		cg_id0 = PQgetvalue(res0,i,cgId0);  
    arquivoalerta=PQgetvalue(res0,i,arquivo0);
	  endbloco0=PQgetvalue(res0,i,kbloco0); 

        if (cgAnt != atoi(cg_id0)){
            if (i!=0){
							 //imprimeLista(sequencia);
               k=comparaRegistros(res1,ntuples1,sequencia,k,cgAnt); 
               if (DEBUG2)printf(" fim compara reg loop\n");
               destroilistaSeq(sequencia); 
               if (DEBUG2)printf(" fim destroi lista loop\n");   
            }
            sequencia=criaEstrutura(arquivoalerta,atoi(endbloco0),ntuples1);
        } 
        else {
            AcrescentaEstrutura(arquivoalerta,atoi(endbloco0),sequencia);
        } 
        cgAnt= atoi(cg_id0);
    }
	//imprimeLista(sequencia);
  comparaRegistros(res1,ntuples1,sequencia,k,atoi(cg_id0));
  if (DEBUG2)printf(" fim compara reg\n");
	PQfinish(conn);
}


listaSeq criaEstrutura(char * nomearquivo,long int endereco, int ntuples1){
  listaSeq sequencia = constroilistaSeq();
  FILE *fp0=fopen(nomearquivo,"rb");
  if (DEBUG2)printf("cria estrutura \n");
  //le cada registro do bloco e insere na lista
  buscaArquivo(fp0,endereco,sequencia);
  if (DEBUG2)printf("fim do busca arquivo\n");
  //imprimeLista(sequencia);
  fclose(fp0);
  if (DEBUG2)printf(" fim cria estrutura \n");
  return sequencia;
}

void AcrescentaEstrutura(char * nomearquivo, int endereco,listaSeq sequencia){
    //le cada registro do bloco e insere na lista
    FILE *fp0=fopen(nomearquivo,"rb");
    buscaArquivo(fp0,endereco,sequencia);
    if (DEBUG2) imprimeLista(sequencia);
    fclose(fp0);
    if (DEBUG2)printf(" fim acrecenta estrutura\n");
}


void blocoComSequencia(FILE *fp1,long int endbloco1,listaSeq sequencia){
   if (DEBUG2)printf(" bloco com seq\n");
  int id,cgid,qtda,i, erro;
  long int end;
    
  fseek(fp1,endbloco1,SEEK_SET);
  if (DEBUG2)printf("fseek %d \n",erro);


  fscanf(fp1,"%ld %d %d %d",&end,&id,&cgid,&qtda);
  if (DEBUG2) printf("%d %d %d \n",id,cgid,qtda);

  int v[qtda];
  for (i=0; i< qtda; i++)
    fscanf(fp1,"%d", &v[i]);
  for (i=0; i< qtda; i++){
    if (DEBUG2)printf("\nBusca registro end %d\n",v[i]);
   //le cada registro do bloco e insere na lista
    RegComSequencia(fp1,v[i],sequencia,i);
  } 

}

void RegComSequencia(FILE *fp1,long int endreg,listaSeq sequencia, int cont){
	int tam, tipo,qtaxy,i;
	char hora[MAXSTR],dia[MAXSTR],separa[5];
	Reg n;
    
  if (cont ==0)fseek(fp1,endreg,SEEK_SET);
  fscanf(fp1,"%d",&tam);
  
  //printf("tam %d\n",tam);

  char nomeRz[tam];
  separa[0]=getc(fp1);

  for (i=0; i < tam ; i++){
    nomeRz[i]=getc(fp1);
  }    
  nomeRz[tam]='\0';
   if (DEBUG2)printf("%d NOME %s",tam,nomeRz);
 
	fscanf(fp1,"%d %s %s %d",&tipo,hora,dia,&qtaxy);
   if (DEBUG2)printf(" %d %s %s %d ",tipo,hora,dia,qtaxy);

	double x[qtaxy];
	double y[qtaxy];

  for (i=0; i < qtaxy; i++){
    fscanf(fp1,"%s %lf %s %lf",separa,&x[i],separa,&y[i]);
    //printf(" %lf %lf ",x[i],y[i]);

  } 
	//comparando o registro que leu com a lista de registros
	for(n=primeiroNoSeq(sequencia); proximoNolistaSeq(n)!=NULL; n=proximoNolistaSeq(n)){
		//se o tamanho do nome eh igual, se intersecta
		if (regIgual(tam,nomeRz,qtaxy,x,y,n)==1){
			printf("Intersect %s %s ",nomeRz,n->nome);
			res+=1;
			printf("res %d\n",res);
		}		
	}
}


int comparaRegistros(PGresult* res1, int ntuples1, listaSeq sequencia, int k, int cg0){
  int j,cg1, cont=0;
  char *cg_id1,*arquivoengarrafamento,*endbloco1;
  int cgId1= PQfnumber(res1, "cg_id");
  int arquivo1 = PQfnumber(res1, "arqe");
	int kbloco1= PQfnumber(res1, "blocoe");
  FILE *fp1;

   if (DEBUG2) printf("compara registro\n");

    for (j=k; j < ntuples1; j++){
      cg_id1 = PQgetvalue(res1,j,cgId1);
      cg1=atoi(cg_id1);

      if (cg1 != cg0)
        return j;

      arquivoengarrafamento=PQgetvalue(res1,j,arquivo1);
      endbloco1=PQgetvalue(res1,j,kbloco1);

      if (DEBUG2) printf("\n ################################### \ncg0 %d cg1 %d end1 %s %s\n\n",cg0,cg1,endbloco1,arquivoengarrafamento);	
      //ver o nome do arquivo
      if (cont==0 )fp1=fopen(arquivoengarrafamento,"rb");
      if (fp1 == NULL) printf("FOPEN FAIL\n");

      blocoComSequencia(fp1,atoi(endbloco1),sequencia);
      if (DEBUG2) printf("\n retorno bloco com sequencia\n");
      cont++;   
    }  
  fclose(fp1);
  if (DEBUG2) printf("\n fecha arquivo\n");
  return j;
}

int consulta2(){
	int ntuples0,i,cg_ant=-1,contido=0, idcgi,registros=0;
	char separa[MAXSTR],reg0[2*MAXSTR],nomeRz0[3*MAXSTR];
	FILE *fp0,*fpAnt;
	PGconn *conn = PQconnectdb("user=mariana dbname=postgis_test");
  static PGresult* res0 = NULL;
  char  arqAntes[30];

  //pegar o primeiro conjunto de x e y do poligono (para o x1,y1) e o terceiro (para o x2,y2) 
  double x10=-48.85,y10=-26.35,x20=-48.80,y20=-26.30;

	res0= PQexec(conn, "select i.arquivoengarrafamento, k.blocoe ,j.cg_id from tabbitmaps100 i,cg_id j, vetor_bloco_engarrafamento100 k where i.periododiafim > 736572 and ST_Intersects(j.geom,'POLYGON((-48.85 -26.35,-48.80 -26.35,-48.80 -26.3,-48.85 -26.3,-48.85 -26.35),(-48.85 -26.35,-48.80 -26.35,-48.80 -26.3,-48.85 -26.3,-48.85 -26.35))'::geography) and j.cg_id=k.cg_id order by cg_id");
	ntuples0=PQntuples(res0);
  printf("%d tuples retun\n", ntuples0);

	char *endbloco0,*arquivoengarrafamento,*idCg,*geom0;
	int arquivo0 = PQfnumber(res0, "arquivoengarrafamento");
	int kbloco0 = PQfnumber(res0, "blocoe"); 
  int cg_id = PQfnumber(res0, "cg_id");


	for(int i=0; i < ntuples0; ++i){

		endbloco0=PQgetvalue(res0,i,kbloco0);
		arquivoengarrafamento=PQgetvalue(res0,i,arquivo0);
    idCg = PQgetvalue(res0,i,cg_id);
    idcgi=atoi(idCg);

		if (i==0){
            strcpy(arqAntes,arquivoengarrafamento);
            fp0=fopen(arquivoengarrafamento,"rb");
            fpAnt=fp0;
        }  
        else if(strcmp(arqAntes,arquivoengarrafamento)!=0) 
            fp0=fopen(arquivoengarrafamento,"rb");


		registros+= buscaArquivo2(fp0,atoi(endbloco0),contido,x10,y10,x20,y20);     

		if(strcmp(arqAntes,arquivoengarrafamento)!=0) 
		    fclose(fpAnt);

        strcpy(arqAntes,arquivoengarrafamento);
        fpAnt=fp0;  
        cg_ant= idcgi;
	}	
  if (fp0)
      fclose(fp0);

	//printf("CONT %d",cont);
  printf("REGISTROS %d \n",registros);
	PQfinish(conn);
	return ntuples0;
}
