//
//  CG.c
//  
//
//  Created by Mariana on 12/12/19.
//
#include "listaseq.h"
#include "CG.h"
#include <libpq-fe.h>
//-------------------------------------------------------------------------------------------


gradeP IniciaPrograma(double *x1, double *x2, double *y1, double *y2, int *l, int *c){
    gradeP grade;
    int tam;

    leituraCoordenadas(x1,x2,y1,y2);
    TamanhoMatriz(l,c);
    grade=TamanhoXeY(*x1,*x2,*y1,*y2,*l,*c);
    printf("Digitar tamanho de Bytes máximo para blocos: \n");
    scanf("%d", &tam);

    grade->tamBytes=tam;
    grade->existebitmap0=0;
    grade->existebitmap1=0;
    grade->existebitmap2=0;

    return grade;
}

void FinalizaPeriodo(gradeP grade){
    int i;
    if (grade->arq0) fclose(grade->arq0);
    if (grade->arq1) fclose(grade->arq1);
    if (grade->arq2) fclose(grade->arq2);

    if (grade->tipo0->blocoCG) free(grade->tipo0->blocoCG);
    if (grade->tipo1->blocoCG) free(grade->tipo1->blocoCG);
    if (grade->tipo2->blocoCG) free(grade->tipo2->blocoCG);

    if (grade->tipo0) free(grade->tipo0);
    if (grade->tipo1) free(grade->tipo1);
    if (grade->tipo2) free(grade->tipo2);

    if ( grade->existebitmap0)
        roaring_bitmap_free(grade->btipo0);
    if ( grade->existebitmap1)
        roaring_bitmap_free(grade->btipo1);
    if ( grade->existebitmap2)
        roaring_bitmap_free(grade->btipo2);

    grade->existebitmap0=0;
    grade->existebitmap1=0;
    grade->existebitmap2=0;

    if (grade->v0) free(grade->v0);
    if (grade->v1) free(grade->v1);
    if (grade->v2) free(grade->v2);

}


void FinalizaPrograma(gradeP grade){
    //FinalizaPeriodo(grade);
    if (grade) free(grade);
    printf("CONT %d",pontos);
}


void leituraCoordenadas(double *x1, double *x2, double *y1, double *y2){
    printf("Digitar Pontos de interesse \n");
    scanf("%lf %lf %lf %lf",x1,y1,x2,y2);
   // printf("Pontos de interesse %lf %lf %lf %lf\n",*x1,*y1,*x2,*y2);

}

void TamanhoMatriz(int *l, int *c){
    printf("Digitar o número de linhas e colunas\n");
    scanf("%d %d",l,c);
   // printf("==============================================\n");
}



gradeP TamanhoXeY(double x1, double x2, double y1,double y2, int l, int c){
    gradeP grade=malloc(sizeof(grad));

    if (x1 > x2){
        double aux;
        aux=x1;
        x1=x2;
        x2=aux;
    }
    if (x1 < 0)
        grade->deslocamentoX=(-1)*x1;
    else
         grade->deslocamentoX=0;

    if (y1 > y2){
        double aux;
        aux=y1;
        y1=y2;
        y2=aux;
    }
    if (y1 < 0)
        grade->deslocamentoY=(-1)*y1;
    else
         grade->deslocamentoY=0;

    //printf("deslocamento x %lf y %lf\n", grade->deslocamentoX,grade->deslocamentoY);

    grade->x1=x1;
    grade->y1=y1;
    grade->x2=x2;
    grade->y2=y2;
    grade->passoX=(x2-x1)/l;
    grade->passoY=(y2-y1)/c;
    grade->l=l;
    grade->c=c;

    //printf("passox %f, x1 %f, x2 %f, linhas %d \n",grade->passoX,grade->x1,grade->x2,grade->l);
    //printf("passoy %f, y1 %f, y2 %f, colunas %d \n",grade->passoY,grade->y1,grade->y2,grade->c);
    return grade;
}


CG criaCGs(gradeP g){
    CG c;
    int i;

    c=malloc(((g->l)*(g->c))*sizeof(cg));
    c->blocoCG=NULL;
    for (i=0; i < (g->l)*(g->c); ++i){
        c[i].blocoCG=NULL;
    }
    return c;
}

vetorBloco criavetor(gradeP g){
    vetorBloco v;
    int i;

    v=malloc(((g->l)*(g->c))*sizeof(cgsBloco));

    for (i=0; i < (g->l)*(g->c); ++i){
        v[i].idCgs=i;
        v[i].listaBlocos = NULL;
    }
    return v;
}


void iniciaCgs(gradeP g, int cont){
    char str[10];
    char nome1[15];
    char nome2[20];
    char nome3[20];

    g->numBlocos0=0;
    g->numBlocos1=0;
    g->numBlocos2=0;

    g->v0=criavetor(g);
    g->v1=criavetor(g);
    g->v2=criavetor(g);

    g->tipo0=criaCGs(g);
    g->tipo1=criaCGs(g);
    g->tipo2=criaCGs(g);

    sprintf(str, "%d",cont);
    sprintf(nome1, "%s","Alerta");
    strcat(nome1,str);
    strcpy(g->filename0,nome1);

    g->arq0=fopen(nome1,"w+");


    sprintf(nome2, "%s","Engarrafamento");
    //printf("%s \n",nome2);
    //printf("%s \n",str);
    strcat(nome2,str);
    
    //printf("%s \n",nome2);
    strcpy(g->filename1,nome2);
    g->arq1=fopen(nome2,"w+");

    sprintf(nome3, "%s","Irregularidade");
    strcat(nome3,str);
    //printf("%s \n",nome3);
    strcpy(g->filename2,nome3);

    g->arq2=fopen(nome3,"w+");
    //printf("fim do inicia CGs\n");
}

int calculoVetor(int numL, int c, int numC){
    return ((numL*c)+numC);
}

void calculaPeriodoBloco(bloco b){
    registroP reg= malloc(sizeof(registro));
    int i;

    b->menorTempo=10000;
    b->maiorTempo=0;

    for(i=0; i< b->numReg ;++i){
        reg=(b->registros[i]);
        if (atoi(reg->tempoDia) < b->menorTempo)
            b->menorTempo=atoi(reg->tempoDia);
        if (atoi(reg->tempoDia) > b->maiorTempo)
            b->maiorTempo=atoi(reg->tempoDia);
    }
}


void gravaRegistro(CG c, FILE *fp, gradeP grade, int tipo){
    bloco b;
    int i, j,k,m;
    long int l;

    for(i=0; i < (grade->l*grade->c); ++i){
       if (c[i].blocoCG != NULL){
           b=c[i].blocoCG;
           arrumaIDbloco(grade,tipo,b);

            calculaPeriodoBloco(b);
            
            
            long int comeco=ftell(fp);
            long int aux=comeco;
            char tam[8];
            //printf("comeco %lu \n",comeco);
            fprintf(fp,"%d %d %d",b->idBloco,b->idCG,b->numReg);
            l=ftell(fp);
            //printf("%ld %d %d %d \n",comeco,b->idBloco,b->idCG,b->numReg);
            arrumaEndbloco(grade,tipo,b,b->idCG,comeco);
             
            aux=0; 
            
            for ( m=0; m < b->numReg ; ++m){
                //fprintf(fp," %ld ",aux);
                fprintf(fp," %d",b->registros[m]->tamTotal);
                sprintf(tam," %d",b->registros[m]->numPonto);
                b->registros[m]->tamTotal+=strlen(tam);
                //printf("TAMAHHO %d\n",b->registros[m]->tamTotal);
                aux+=b->registros[m]->tamTotal;
            }
           // fprintf(fp,"%ld",aux);
            long int fim = ftell(fp);
            //printf("fim %lu \n",fim);

            int NUMS=labs(fim - (comeco+Header));
            if (((fim - (comeco+Header))-1) > 0){
                printf("aumentar o tamanho do header em %d",NUMS);
                printf(" NUM REGS %d idBloco %d tipo %d \n",b->numReg,b->idBloco, b->registros[0]->tipo);
            }    

            //printf("Nums %d\n",NUMS);
            for (m=0; m < NUMS;m++)
                 fprintf(fp," ");

            fprintf(fp,"\n");

            for (j=0; j < b->numReg ; ++j){
                //printf(" %s tipo %d tempo %s Ftell %ld\n",b->registros[j]->nome,b->registros[j]->tipo, b->registros[j]->tempoDia,ftell(fp));

                fprintf(fp,"%d %s %s %s %d",b->registros[j]->tamNome,b->registros[j]->nome, b->registros[j]->tempoDia,b->registros[j]->tempoHora,b->registros[j]->numPonto);

                for(k=0; k < b->registros[j]->numPonto; ++k){
                    //printf("numPonto %d\n",b->registros[j].numPonto);
                    fprintf(fp," %lf %lf ",b->registros[j]->x[k],b->registros[j]->y[k]);
                    //printf("Indice %d x %f y %f\n",k,b->registros[j]->x[k],b->registros[j]->y[k]);
                }
              fprintf(fp,"\n");
            }
        }
    }
    //printf("fim do arquivo\n");
}

void imprimeVetor(vetorBloco v, int tipo, gradeP g){
    int i;
    no n;
    printf("\n//////////////////////\n");
    //fprintf(fp,"\n//////////////////////\n");

    printf("Vetor %d\n", tipo);
    //fprintf(fp,"Vetor %d\n", tipo);

    for (i=0; i < (g->l)*(g->c); ++i){
        if (v[i].listaBlocos!= NULL){
            printf("CG %d\n",i);
            //fprintf(fp,"CG %d\n",i);

            for (n=primeiroNo(v[i].listaBlocos); n!=NULL; n=proximoNo(n)){
                printf("Endereco bloco %ld\n",conteudo(n));
                //fprintf(fp,"Endereco bloco %ld\n",conteudo(n));
            }
            printf("------------------------\n");
            //fprintf(fp,"------------------------\n");
        }
    }
    printf("//////////////////////\n");
    //fprintf(fp,"+++++++++++++++++++++++++\n");
}

void imprimeBitmap(roaring_bitmap_t *bit, int tipo, gradeP g){
    roaring_bitmap_printf(bit);
}


void gravaRegistrosFimLeitura(gradeP grade, int num,PGconn *conn){
    if (TESTEBANCO) criaTabelaRegistros(grade,conn);

    gravaRegistro(grade->tipo1, grade->arq1,grade,1);
    gravaRegistro(grade->tipo0, grade->arq0,grade,0);
    gravaRegistro(grade->tipo2,grade->arq2,grade,2);

    printf("&&&&&&&&&&&&&&&\n");
    criaBancoGCs(grade,num,conn);
    printf("&&&&&&&&&&&&&&&\n");
   
}

void insereBlocoVetor(vetorBloco v, long int end, int celula){
    if (v[celula].listaBlocos == NULL)
        v[celula].listaBlocos= constroiLista();
    
    insereLista(end,v[celula].listaBlocos);
}



void arrumaIDbloco(gradeP g, int tipo, bloco b){
    switch (tipo){
        case 0: b->idBloco=g->numBlocos0; break;
        case 1: b->idBloco=g->numBlocos1; break;
        case 2: b->idBloco=g->numBlocos2; break;
    }
}

void arrumaEndbloco(gradeP g, int tipo, bloco b, int celula, long int end){
    switch (tipo){
        case 0: insereBlocoVetor(g->v0,end,celula);(g->numBlocos0)++;break;
        case 1: insereBlocoVetor(g->v1,end,celula);(g->numBlocos1)++;break;
        case 2: insereBlocoVetor(g->v2,end,celula);(g->numBlocos2)++; break;
    }
}



void insereNoCG(registroP r, CG vetor, gradeP g, FILE *fp,PGconn *conn){
    //printf("insere no cg\n");
    int indice=calculoVetor(r->numX,g->c,r->numY);
    long int l;

    bloco b=vetor[indice].blocoCG;
    switch (r->tipo){
        case 0: RuaCGs(g,r,g->btipo0,g->existebitmap0);break;
        case 1: RuaCGs(g,r,g->btipo1,g->existebitmap1);break;
        case 2: RuaCGs(g,r,g->btipo2,g->existebitmap2);break;
    }

    //printf("//--------------\n");
    if (vetor[indice].blocoCG == NULL){
        //printf("indice vazio\n");
        vetor[indice].blocoCG=malloc(sizeof(bloco));
        bloco b=malloc(sizeof(Block));
        b->idCG=indice;
        b->numReg=0;
        b->tamGravado=0;
        vetor[indice].blocoCG=b;
        vetor[indice].blocoCG->registros=malloc(g->tamBytes*sizeof(registroP));
    }
    else{
        //printf("entrou no else \n");
        //verifica se chegou no limite do conteudo, grava e descarrega
        if (((b->tamGravado) + r->tamTotal) > g->tamBytes){
            int i,j,m;
            arrumaIDbloco(g,r->tipo,b);
            calculaPeriodoBloco(b);
    
            long int comeco=ftell(fp);
            long int aux=comeco;
            char tam[8];
            //header
            fprintf(fp,"%d %d",b->idBloco,b->numReg);
            l=ftell(fp);
       
            arrumaEndbloco(g,r->tipo,b,b->idCG,comeco);
            aux=0; 
            
            for (m=0; m < b->numReg ; ++m){
                //fprintf(fp," %ld ",aux);
                fprintf(fp," %d",b->registros[m]->tamTotal);
                sprintf(tam," %d",b->registros[m]->numPonto);
                
                b->registros[m]->tamTotal+=strlen(tam);
                aux+=b->registros[m]->tamTotal;
                
            }

            //fprintf(fp,"%ld",aux);
            int fim = ftell(fp);
            //printf("fim %d \n",fim);

            int NUMS=labs(fim-(comeco+Header));

            //printf("Nums %d\n",NUMS);

            if (((fim - (comeco+Header))-1) > 0){
                printf("aumentar o tamanho do header em %d\n",NUMS);
                printf(" NUM REGS %d idBloco %d tipo %d \n",b->numReg,b->idBloco, r->tipo);
                //exit(0);
            }    

            for (m=0; m < NUMS;m++)
                 fprintf(fp," ");

            fprintf(fp,"\n");

           // printf("aqui %d\n",b->numReg);

            for (i=0; i < b->numReg ; ++i){
                //printf(" %s tipo %d tempo %s Ftell %ld\n",b->registros[i]->nome,b->registros[i]->tipo, b->registros[i]->tempoDia,ftell(fp));

                fprintf(fp,"%d %s %s %s %d",b->registros[i]->tamNome,b->registros[i]->nome, b->registros[i]->tempoDia, b->registros[i]->tempoHora,b->registros[i]->numPonto );

                for(j=0; j < b->registros[i]->numPonto; ++j){
                    fprintf(fp," %lf %lf ",b->registros[i]->x[j],b->registros[i]->y[j]);
                    //printf("Indice %d x %f y %f\n",j,b->registros[i]->x[j],b->registros[i]->y[j]);
                }
                fprintf(fp,"\n");
            }

            if (TESTEBANCO) {
           
                TabelaRegistros(g,b,b->registros[0]->tipo,conn);

            }    

            if (b->numReg == 0 )
                printf("Tamanho de Bloco muito pequeno!!!\n");
             //printf("tam gravado %d tamR %lu tamdefinido %d\n",b->tamGravado,sizeof(r), g->tamBytes);

            if(b) free(b);
            if (vetor[indice].blocoCG){
                free(vetor[indice].blocoCG->registros);
                vetor[indice].blocoCG=malloc(sizeof(bloco));
            }

            bloco b=malloc(sizeof(Block));
            b->idCG=indice;
            b->numReg=0;
            b->tamGravado=0;
            vetor[indice].blocoCG=b;
            vetor[indice].blocoCG->registros=malloc(g->tamBytes*sizeof(registroP));
        }
    }
    //senao vai inserir

    (vetor[indice].blocoCG->numReg)++;

    (vetor[indice].blocoCG->tamGravado)+= r->tamTotal;

    vetor[indice].blocoCG->registros[(vetor[indice].blocoCG->numReg)-1]=r;

    vetor[indice].blocoCG->idCG=indice;

}



void imprimeBloco(bloco b, int tam){
    int i;
    printf("BLOCO IDCG %d TamGravado %d NumReg %d\n",b->idCG,b->tamGravado,b->numReg);
    for (i=0; i < tam ; ++i){
        printf("Registro tipo %d x %f y %f\n",b->registros[i]->tipo,b->registros[i]->x[0],b->registros[i]->y[0]);
    }
}




//aloca registro e verifica tipo
int alocacaoPonto(int tipo, char nome[Nome], char tempo[Nome], gradeP grade, double x1, double y1, char line[MAXSTR],int tamSub, PGconn *conn){
    //alocar registro

    registroP reg=malloc(sizeof(registro));
    reg->numPonto=0;
    reg->tamTotal=0;

    lerPeriodo(tempo,reg);
    if (verificaCG(reg,x1,y1,grade,nome)==0)
        return 0;

    //verificar tipo
    switch(tipo){
        case 0: leituraAlerta(reg,grade,line,tamSub,conn); break;
        case 1: leituraEngarrafamento(reg,grade,line,tamSub,conn); break;
        case 2: leituraIrregularidade(reg, grade,line, tamSub,conn); break;
        default : break;
    }
    return 1;
}


int verificaX(gradeP grade, double x){
    
    return floor(((x)-(grade->x1))/grade->passoX);
}

int verificaY(gradeP grade, double y){
   
    return floor(((y)-(grade->y1))/grade->passoY);
}


//verifica a CG a qual o registro pertence
int verificaCG(registroP reg, double x, double y, gradeP grade, char * nome){
    //verifica se ponto esta na area de interesse
    char aux[MAXSTR];
    char aux2[MAXSTR];

    int off,i;
    if ( (x > grade->x2) || (x < grade->x1) || (y > grade->y2) || (y < grade->y1)){
        int tipo;
        //printf("Ponto fora da area de interesse\nDigite Novamente %lf %lf \n X1 %lf Y1 %lf X2 %lf Y2 %lf\n", x,y,grade->x1,grade->y1,grade->x2,grade->y2);
        //quantidade de pontos fora da area de interesse
        pontos++;
        return 0;
    }
    reg->numX = verificaX(grade,x);

    reg->numY= verificaY(grade,y);

    strcpy(reg->nome,nome);
    reg->tamNome=strlen(reg->nome);
  
    reg->x[reg->numPonto]=x;
    reg->y[reg->numPonto]=y;
    (reg->numPonto)++;

    //calculo do tamanho do registro para o header ---------------------------------------
    sprintf(aux,"%d %s %d %s %s",reg->tamNome,nome,reg->tipo,reg->tempoDia,reg->tempoHora);
    off=strlen(aux);
    sprintf(aux+off," x %lf y %lf ",x,y);
    sprintf(aux2," x %lf y %lf ",x,y);

    off=strlen(aux);
    int off2=strlen(aux2);
    sprintf(aux+off,"\n");
    //tamanho do registro (com somente o primeiro ponto e sem a quantidade de pontos)
    reg->tamTotal=strlen(aux);
    return 1;
}

double calculaDistancia(double xinter, double yinter, double x1, double y1){
    return (pow((xinter-x1),2) + pow((yinter-y1),2));
}


registroP inserePontoIntermediario(registroP reg,gradeP grade, double x2, double y2){
    //quando muda de CG é preciso verificar se existem pontos adicionais entre p2 e p1
   
    int numPontoX=0,numPontoY=0;
    double xinter,yinter, xinterCopia, yinterCopia, b,m,distancia1=0, distancia2=0;

    b=(((y2* reg->x[reg->numPonto-1])-(reg->y[reg->numPonto-1]*x2))/(reg->x[reg->numPonto-1]-x2));
    
    m=(reg->y[reg->numPonto-1]- b)/(reg->x[reg->numPonto-1]);

    //C
    if (reg->x[reg->numPonto-1] == x2){
        xinter=reg->x[reg->numPonto-1];

        if (reg->y[reg->numPonto-1] < y2)
            yinter= ceil((reg->y[reg->numPonto-1] - grade->y1)/grade->passoY);
        else
            yinter=floor((reg->y[reg->numPonto-1] - grade->y1)/grade->passoY);
    
        yinter*=(grade->passoY);
        xinterCopia=xinter;
        yinterCopia=yinter;

    }
    //C
    else if (reg->y[reg->numPonto-1] == y2){
        yinter=reg->y[reg->numPonto-1];
        yinterCopia=yinter;

        if (reg->x[reg->numPonto-1] < x2 ){
            xinter= floor((reg->x[reg->numPonto-1] - grade->x1)/grade->passoX);
            xinter*=(grade->passoX);
            xinterCopia=xinter-0.0001;
        }
        else {
            xinter= ceil((reg->x[reg->numPonto-1] - grade->x1)/grade->passoX);
            xinter*=(grade->passoX);
            xinterCopia=xinter;
        }
    }
    //A
    else if (reg->x[reg->numPonto-1] > x2){

        yinter= ceil((reg->y[reg->numPonto-1] - grade->y1)/grade->passoY);
        yinter*=(grade->passoY);
        xinter=(yinter - b)/m;
        xinterCopia=xinter;
        yinterCopia=yinter;
        distancia1= calculaDistancia(xinter, yinter,reg->x[reg->numPonto-1],reg->y[reg->numPonto-1]);
        if (distancia1 == 0 )
            distancia1=10000000000000000;

        //printf("xint %f distancia %f\n",xinter, distancia2);
        if (reg->y[reg->numPonto-1] < y2){
            xinter= floor((reg->x[reg->numPonto-1] - grade->x1)/grade->passoX);
            xinter*=(grade->passoX);
            yinter=(xinter * m )+ b;
        }
        else {
            xinter= ceil((reg->x[reg->numPonto-1] - grade->x1)/grade->passoX);
            xinter*=(grade->passoX);
            yinter=(xinter * m )+ b;
        }
        distancia2= calculaDistancia(xinter, yinter,reg->x[reg->numPonto-1],reg->y[reg->numPonto-1]);
         if (distancia2== 0 )
            distancia2=10000000000000000;

        if (distancia1 < distancia2){
            //printf("distancia1 < distancia2)\n");
            xinter=xinterCopia;
            xinterCopia+=0.0001;
            yinter=yinterCopia;
        }
        else {
            xinterCopia=xinter;
            if (reg->y[reg->numPonto-1] < y2)
               xinterCopia=xinter-0.0001;
            yinterCopia=yinter;
        }

    }
    //B
    else if (reg->x[reg->numPonto-1] < x2){

        yinter= floor((reg->y[reg->numPonto-1] - grade->y1)/grade->passoY);
        yinter*=(grade->passoY);
        xinter=(yinter - b)/m;
        xinterCopia=xinter;
        yinterCopia=yinter;
        distancia1= calculaDistancia(xinter, yinter,reg->x[reg->numPonto-1],reg->y[reg->numPonto-1]);
        if (distancia1 == 0 )
            distancia1=10000000000000000;

        if (reg->y[reg->numPonto-1] > y2){
            xinter= ceil((reg->x[reg->numPonto-1] - grade->x1)/grade->passoX);
            xinter*=(grade->passoX);
            yinter= (xinter * m )+ b;
        }
        else {
            xinter= floor((reg->x[reg->numPonto-1] - grade->x1)/grade->passoX);
            xinter*=(grade->passoX);
            yinter=(xinter * m )+ b;
        }
        distancia2= calculaDistancia(xinter, yinter,reg->x[reg->numPonto-1],reg->y[reg->numPonto-1]);
        if (distancia2== 0 )
            distancia2=10000000000000000;

        if (distancia1 < distancia2){
            xinter=xinterCopia;
            yinter=yinterCopia;
        }
        else {
            xinterCopia=xinter;
            yinterCopia=yinter;
        }

    }
    
    //printf("Ponto intermediario X %f Y %f *************************************************\n",xinter, yinter);

    if (reg->numPonto < valor){
        reg->x[(reg->numPonto)]=xinter;
        reg->y[(reg->numPonto)]=yinter;
        (reg->numPonto)++;
        
        //somando mais os pontos inseridos no tamanho do registro para o header
        char aux[60];
        sprintf(aux," x %lf y %lf ",xinter,yinter);
        int tam=strlen(aux);

        (reg->tamTotal)+=tam;
    }

    registroP reg2=malloc(sizeof(registro));
    reg2->numPonto=0;
    reg2->tamTotal=0;

    reg2->tipo=reg->tipo;
    strcpy(reg2->tempoDia,reg->tempoDia);
    strcpy(reg2->tempoHora,reg->tempoHora);
    
    strcpy(reg2->nome,reg->nome);


    if (verificaCG(reg2,xinterCopia,yinterCopia,grade,reg->nome)==0) return NULL;
    return reg2;
    
}


int verificaPontos(registroP reg,gradeP grade, int tipo, double x2, double y2, PGconn *conn){
    int numCG, numx2, numy2;
   // printf("VERIFICA PONTO x1 %f y1 %f x2 %f y2 %f\n",reg->x[reg->numPonto-1],reg->y[reg->numPonto-1],x2,y2);
    //ver em qual CG o proximo ponto esta

    numx2=verificaX(grade,x2);
    numy2=verificaY(grade,y2);

    if (numx2 != reg->numX || numy2!= reg->numY){
        //trocando de CG - cria um novo regitro e grava o registro anterior
        registroP reg2;
        reg2=inserePontoIntermediario(reg,grade,x2,y2);
        //printf("vai chamar verifica ponto recursivo\n");
        if (reg2 == NULL)
            return 0;
        verificaPontos(reg2,grade,reg2->tipo,x2,y2,conn);
 

        if (tipo == 1)
            insereNoCG(reg2,grade->tipo1,grade,grade->arq1,conn);
        else if (tipo == 2)
            insereNoCG(reg2,grade->tipo2,grade,grade->arq2,conn);
        
    }
    else {//proximo ponto esta no mesmo CG que
        //se cabe mais um ponto no vetor de pontos
        if (reg->numPonto < valor){
            reg->x[reg->numPonto]=x2;
            reg->y[reg->numPonto]=y2;

            //somando mais os pontos inseridos no tamanho do registro para o header
            char aux[60];
            sprintf(aux," x %lf y %lf ",x2,y2);
            int tam=strlen(aux);
            (reg->tamTotal)+=tam;

            ++(reg->numPonto);

        }
        else {
            //nao cabe o ponto pelo valor definido
            //grava registro no CG
            
            //cria novo registro com o mesmo CG, com P1 e P2
            registroP reg2=malloc(sizeof(registro));
            reg2->numPonto=0;
            
            strcpy(reg2->nome,reg->nome);
            strcpy(reg2->tempoDia,reg->tempoDia);
            strcpy(reg2->tempoHora,reg->tempoHora);

            verificaCG(reg2,reg->x[(reg->numPonto)-1],reg->y[(reg->numPonto)-1],grade,reg->nome);

            //inserere o P2 no segundo registro
            reg2->x[reg2->numPonto]=x2;
            reg2->y[reg2->numPonto]=y2;
            reg2->tipo=tipo;
            //printf("nome %s %d novo x %f y %f\n",reg2->nome,reg2->tipo,reg2->x[reg2->numPonto],reg2->y[reg2->numPonto]);
            (reg2->numPonto)++;

            //somando mais os pontos inseridos no tamanho do registro para o header
            char aux[60];
            sprintf(aux," x %lf y %lf ",x2,y2);
            int tam=strlen(aux);
            (reg2->tamTotal)+=tam;

            if (tipo == 1)
                insereNoCG(reg2,grade->tipo1,grade,grade->arq1,conn);
            else if (tipo == 2)
                insereNoCG(reg2,grade->tipo2,grade,grade->arq2,conn);

        }
    }
    return 1;
}

//verifica pontos adicionais
int loopVerificaPontos(registroP reg, gradeP grade, int tipo, char line[MAXSTR], int tamSub, PGconn *conn){
    char s=line[tamSub];
    double x2,y2;
    char separa1[MAXSTR/2], separa2[MAXSTR/2],separa3[MAXSTR/2],separa4[MAXSTR/2],sub[MAXSTR], separa5[MAXSTR/2];
    char *linep=malloc(MAXSTR*sizeof(char));
    linep=line;
    int lenSub=tamSub;

    while ((s!='\n' || s!= ']') &&( lenSub < strlen(line))){
        sscanf(linep+lenSub,"%s %lf %s %s %lf %s",separa2,&x2,separa3,separa4,&y2,separa5);

        sprintf(sub,"%s %lf %s %s %lf %s",separa2,x2,separa3,separa4,y2,separa5);
        lenSub+=strlen(sub);

        s=line[lenSub];
        if (verificaPontos(reg,grade,tipo, x2,y2,conn)==0)
            return 0;
        
    }
    return 1;
}


void leituraAlerta(registroP reg, gradeP grade,char line[MAXSTR], int tamSub, PGconn *conn){

    reg->tipo=0;
    reg->tam=TAMREG;
   
    insereNoCG(reg,grade->tipo0,grade,grade->arq0,conn);

}

int leituraEngarrafamento(registroP reg, gradeP grade, char line[MAXSTR], int tamSub,PGconn *conn){
    reg->tipo=1;
    reg->tam=TAMREG;
    if (loopVerificaPontos(reg,grade,1,line,tamSub,conn)==0)
        return 0;

    insereNoCG(reg,grade->tipo1,grade,grade->arq1,conn);
    return 1;
}


int leituraIrregularidade(registroP reg, gradeP grade, char line[MAXSTR],int tamSub,PGconn *conn){
    reg->tipo=2;
    reg->tam=TAMREG;
    if(loopVerificaPontos(reg,grade,2,line,tamSub,conn)==0)
        return 0;
    insereNoCG(reg,grade->tipo2,grade,grade->arq2,conn);
    return 1;
}


void RuaCGs(gradeP g, registroP r, roaring_bitmap_t *bitmap, int existe){
    int linCol=g->l*g->c,num;
    size_t tam=linCol;

    //se nao existir o indice bitmap
    if (!existe){
        bitmap= roaring_bitmap_create_with_capacity(linCol);
        //inserindo o numero de Gcs para o vetor de bit

        switch (r->tipo){
            case 0:g->btipo0=bitmap; g->existebitmap0=1;break;
            case 1:g->btipo1=bitmap; g->existebitmap1=1;break;
            case 2:g->btipo2=bitmap; g->existebitmap2=1;break;
        }
    }
    //adicionando o bit no CG
    num=calculoVetor(r->numX,g->c,r->numY);
    roaring_bitmap_add((bitmap),num);
}




void lerPeriodo(char *tempo, registroP reg){
  sscanf(tempo, "%s %s",reg->tempoDia,reg->tempoHora);
}


int achaMenor(long int calc0,long int calc1, long int calc2,int fimArquivo0, int fimArquivo1, int fimArquivo2, int usadoI, int usadoJ,int usadoK){
    if (calc0 <= calc1 && calc0 <= calc2 && !fimArquivo0 && !usadoI)
        return 0;
    else if(calc1 <= calc0 && calc1 <= calc2 && !fimArquivo1 && !usadoJ)
        return 1;
    else if (!fimArquivo2 && !usadoK)
        return 2;
    else return -1;
}

void TabelaRegistros(gradeP g,bloco b, int tipo,PGconn * conn){
        char info[MAXSTR];  
        char geom[MAXSTR*3];
        char command[MAXSTR*5];
        int j,k,off=0;
        static PGresult* res0 = NULL;
        char str[72];
        switch (tipo) {
            case 0: res0 = PQexec(conn,"CREATE TABLE IF NOT EXISTS RegistrosAler (nome text, pub_utc_date timestamp without time zone, ID_CG int, geom geometry(Point));");strcpy(str,"INSERT INTO RegistrosAler (nome, pub_utc_date, ID_CG, geom) VALUES (");break;
            case 1: res0 = PQexec(conn,"CREATE TABLE IF NOT EXISTS RegistrosJam (nome text, pub_utc_date timestamp without time zone, ID_CG int, geom geometry(LineString));");strcpy(str,"INSERT INTO RegistrosJam (nome, pub_utc_date, ID_CG, geom) VALUES (");break;
            case 2: res0 = PQexec(conn,"CREATE TABLE IF NOT EXISTS RegistrosIrr (nome text, pub_utc_date timestamp without time zone, ID_CG int, geom geometry(LineString));");strcpy(str,"INSERT INTO RegistrosIrr (nome, pub_utc_date, ID_CG, geom) VALUES (");break;
        }

        for (j=0; j < b->numReg ; ++j){  
                   
            sprintf(info,"'%s','%s %s'",b->registros[j]->nome,b->registros[j]->tempoDia,b->registros[j]->tempoHora);          

            //alerta
            if (tipo == 0){
                //imprimir em forma de geom
                sprintf(geom,"'POINT(%lf %lf)'",b->registros[j]->x[0],b->registros[j]->y[0]);
            }    
            else{    
                for(k=0; k < b->registros[j]->numPonto; ++k){
                    if( k ==0 ) sprintf(geom,"'LINESTRING(%lf %lf",b->registros[j]->x[k],b->registros[j]->y[k]);
                    off=strlen(geom);
                    sprintf(geom+off,", %lf %lf",b->registros[j]->x[k],b->registros[j]->y[k]);
                } 
                sprintf(geom+off,")'");
            }      
            //printf("%s\n",geom);    
            sprintf(command,"%s%s,%d,%s);",str,info,b->idCG,geom);
            //printf("%s\n",command);   
            res0 = PQexec(conn,command);   
        }          
    
}

void regsTab(CG c,int tipo,gradeP g, PGconn *conn){
    int i;
    bloco b;
    for(i=0; i < (g->l*g->c); ++i){
       if (c[i].blocoCG != NULL){   
           b=c[i].blocoCG;
           TabelaRegistros(g,b,tipo,conn);
       }
    }
}


void criaTabelaRegistros(gradeP g, PGconn *conn){
    regsTab(g->tipo0,0,g,conn);
    regsTab(g->tipo1,1,g,conn);
    regsTab(g->tipo2,2,g,conn);

}

//cria tabela para vetores de bloco
void criaBancoGCs(gradeP grade,int cont, PGconn *conn){

    static PGresult* res0 = NULL;
    char str[39]="CREATE TABLE IF NOT EXISTS Vetor_bloco_";
    char type0[8]="Alerta\0";
    char type1[15]="Engarrafamento\0";
    char type2[16]="Irregularidades\0";
    char str1[80]="( arquivo integer NOT NULL ,CG_id integer NOT NULL, bloco";
    char stralerta[21]="a integer NOT NULL);\0";
    char streng[21]="e integer NOT NULL);\0";
    char strirr[21]="i integer NOT NULL);\0";
    char chd0[150];


    sprintf(chd0, "%s%s %s%s",str,type0,str1,stralerta);
    //printf("%s\n",chd0);
    //vetor bloco Alerta
    res0 = PQexec(conn,chd0);
    armazenaBanco(grade->v0,0,cont,grade,conn);
    //printf("5\n");
    res0 = PQexec(conn,"CREATE INDEX arqa_indx ON public.vetor_bloco_alerta USING btree(arquivo) TABLESPACE pg_default;");
    res0 = PQexec(conn,"ALTER TABLE public.vetor_bloco_alerta CLUSTER ON arqa_indx;");
    res0 = PQexec(conn,"CREATE INDEX CGa_indx ON public.vetor_bloco_alerta USING btree(CG_ID) TABLESPACE pg_default;");
    res0 = PQexec(conn,"ALTER TABLE public.vetor_bloco_alerta CLUSTER ON CGa_indx;");
     //printf("6\n");
   
    sprintf(chd0, "%s%s %s%s",str,type1,str1,streng);
    //printf("%s\n",chd0);
    //vetor bloco Engarrafamento
    res0 = PQexec(conn,chd0);
    //printf("7\n");
    armazenaBanco(grade->v1,1,cont,grade,conn);
     //printf("8\n");
    res0 = PQexec(conn,"CREATE INDEX arqe_indx ON public.vetor_bloco_engarrafamento USING btree(arquivo) TABLESPACE pg_default;");
    res0 = PQexec(conn,"ALTER TABLE public.vetor_bloco_engarrafamento CLUSTER ON arqe_indx;");
    res0 = PQexec(conn,"CREATE INDEX CGe_indx ON public.vetor_bloco_engarrafamento USING btree(CG_ID) TABLESPACE pg_default;");
    res0 = PQexec(conn,"ALTER TABLE public.vetor_bloco_engarrafamento CLUSTER ON CGe_indx;");

    sprintf(chd0, "%s%s %s%s",str,type2,str1,strirr);
    //printf("%s\n",chd0);
    //vetor bloco Irregularidades
    res0 = PQexec(conn,chd0);
    armazenaBanco(grade->v2,2,cont,grade,conn);
    res0 = PQexec(conn,"CREATE INDEX arqi_indx ON public.vetor_bloco_irregularidades USING btree(arquivo) TABLESPACE pg_default;");
    res0 = PQexec(conn,"ALTER TABLE public.vetor_bloco_irregularidades CLUSTER ON arq_indx;");
    res0 = PQexec(conn,"CREATE INDEX CGi_indx ON public.vetor_bloco_irregularidades USING btree(CG_ID) TABLESPACE pg_default;");
    res0 = PQexec(conn,"ALTER TABLE public.vetor_bloco_irregularidades CLUSTER ON CGi_indx;");



    armazenaTabBitmap(grade,conn);

    printf("fim cria bancos\n");
}

char * lerBitmap(FILE *fp){
    int bit;
    char abre[2];
    char virgula[2];
    char *str=malloc(MAXSTR*sizeof(char));
    char *str2=malloc(2*MAXSTR*sizeof(char));

    fscanf(fp,"%s %d %s",abre,&bit,virgula);

    sprintf(str,"%s%d%s",abre,bit,virgula);

    while (strcmp(virgula,"}")!=0){
        fscanf(fp,"%d %s",&bit,virgula);
        sprintf(str2,"%d%s",bit,virgula);
        strcat(str,str2);

    }
    return (str);
}


void armazenaTabBitmap(gradeP g,PGconn *conn){
    static PGresult* res0 = NULL;
    char str[200]="INSERT INTO TabBitmaps ( PeriodoDia, PeriodoDiaFIM, alerta, engarrafamento, irregularidade, arquivoAlerta, arquivoEngarrafamento,arquivoIrregularidades) VALUES (\0";
    char chd0[6*MAXSTR];
    char bitmapAler[MAXSTR];
    char bitmapEng[MAXSTR];
    char bitmapIRR[MAXSTR];

    FILE *fp=fopen("bitmaps","w+");
    if (g->btipo0)
        roaring_bitmap_printf_file(g->btipo0,fp);
    if (g->btipo1)
        roaring_bitmap_printf_file(g->btipo1,fp);
    if (g->btipo2)
        roaring_bitmap_printf_file(g->btipo2,fp);

    fclose(fp);
    fp=fopen("bitmaps","r");
    //printf("bitmap\n");
    //imprime bitmaps
    if (g->btipo0)
        strcpy(bitmapAler,lerBitmap(fp));
    else  strcpy(bitmapAler,"NULL");
    if (g->btipo1)
        strcpy(bitmapEng,lerBitmap(fp));
    else  strcpy(bitmapEng,"NULL");
    if (g->btipo2)
        strcpy(bitmapIRR,lerBitmap(fp));
    else  strcpy(bitmapIRR,"NULL");
    
    fclose(fp);

    sprintf(chd0, "%s%ld, %ld,rb_build('%s'),rb_build('%s'),rb_build('%s'),'%s','%s','%s');",str,g->periodoDia,g->periodoDia+g->periodo,bitmapAler,bitmapEng,bitmapIRR,g->filename0,g->filename1,g->filename2);

    res0 = PQexec(conn,chd0);

    //printf("bitmap\n");
}

//vetores de bloco
void armazenaBanco(vetorBloco v, int tipo, int cont, gradeP g,PGconn *conn){
    int i,off=0,j;
    no n;
    static PGresult* res0 = NULL;
    char str[26]="INSERT INTO Vetor_bloco_\0";
    char type[16];
    char type0[8]="Alerta\0";
    char type1[15]="Engarrafamento\0";
    char type2[16]="Irregularidades\0";
    char str1[32]="(arquivo ,CG_id, bloco";
    char strcomplete[10];
    char str2[200*MAXSTR];
    char chd0[200*MAXSTR+100];


    switch (tipo){
        case 0:strcpy(type,type0);strcpy(strcomplete,"a) VALUES"); if (DEBUG)imprimeVetor(g->v0,0,g);break;
        case 1:strcpy(type,type1);strcpy(strcomplete,"e) VALUES");if (DEBUG)imprimeVetor(g->v1,1,g);break;
        case 2:strcpy(type,type2);strcpy(strcomplete,"i) VALUES");if (DEBUG)imprimeVetor(g->v2,2,g);break;
    }
   
    //inserindo vetores de bloco
    //printf("2\n");
    for (i=0; i < (g->l)*(g->c); ++i){
        
        if (v[i].listaBlocos!= NULL){
            for (j=1,n=primeiroNo(v[i].listaBlocos); j <= tamanhoLista(v[i].listaBlocos); n=proximoNo(n),++j){
                if ( j < tamanhoLista(v[i].listaBlocos) )
                    sprintf(str2+off,"(%d, %d, %ld),",cont,i,conteudo(n));
                else
                    sprintf(str2+off,"(%d, %d, %ld)",cont,i,conteudo(n));

                off+=strlen(str2+off);
                //printf("3\n");
            }
           // printf("4\n");
            //printf("%s%s %s%s %s\n",str,type,str1,strcomplete,str2);
            sprintf(chd0, "%s%s %s%s %s;",str,type,str1,strcomplete,str2);
            res0 = PQexec(conn,chd0);
            //printf("4-1\n");
        }
        off=0;
    }
    
}


void deletaBancoGCs(gradeP grade,int cont, PGconn *conn){
    static PGresult* res0 = NULL;
    char str[39]="DROP TABLE Vetor_bloco_";
    char type0[8]="Alerta\0";
    char type1[15]="Engarrafamento\0";
    char type2[16]="Irregularidades\0";
    char chd0[150];

    sprintf(chd0, "%s%s;",str,type0);
    
    //vetor bloco Alerta
    res0 = PQexec(conn,chd0);
   
    sprintf(chd0, "%s%s;",str,type1);
   
    //vetor bloco Engarrafamento
    res0 = PQexec(conn,chd0);

    sprintf(chd0, "%s%s;",str,type2);
   
    //vetor bloco Irregularidades
    res0 = PQexec(conn,chd0);
    res0 = PQexec(conn,"DROP TABLE TabBitmaps;");
    res0 = PQexec(conn,"DROP TABLE CG_ID;");

}


//cria tabela que armazena bitmaps por periodo
void criaBancoInicio(gradeP g,PGconn *conn){
    
    static PGresult* res0 = NULL;

    res0 = PQexec(conn,"CREATE TABLE IF NOT EXISTS TabBitmaps ( PeriodoDia integer NOT NULL, PeriodoDiaFIM integer NOT NULL, alerta roaringbitmap, engarrafamento roaringbitmap, irregularidade roaringbitmap, arquivoAlerta TEXT, arquivoEngarrafamento TEXT,arquivoIrregularidades TEXT );");

}

//cria tabela de Cgs para realizar bitmap com a tab de ruas (quais Cgs a rua pertence)
void criaTabCGsFixa(gradeP g, PGconn *conn){
    static PGresult* res0 = NULL;
    char str[58]="INSERT INTO CG_ID (CG_id, geom) VALUES\0";
    char values[3000];
    char chd0[40000];
    int i;
    int linha,coluna;
    double x1=g->x1,y1=g->y1,x2,y2;
    int mudaLinha=1;

    res0 = PQexec(conn,"CREATE TABLE IF NOT EXISTS CG_ID( CG_id integer NOT NULL, geom geometry(POLYGON));");
    for (i=0; i< (g->l)*(g->c);++i){

        linha = floor(i/g->c);
        coluna = i - floor(i/g->c)* g->c;
        
        x2 = g->x1 + (linha + 1)*g->passoX;
        y2 = g->y1 + g->passoY * (coluna +1);

        sprintf(values,"(%d ,'POLYGON(( %lf %lf , %lf %lf , %lf %lf , %lf %lf , %lf %lf ),( %lf %lf , %lf %lf , %lf %lf , %lf %lf , %lf %lf ))')",i,x1,y1,x2,y1,x2,y2,x1,y2,x1,y1,x1,y1,x2,y1,x2,y2,x1,y2,x1,y1);
        sprintf(chd0, "%s%s;",str,values);

        res0 = PQexec(conn,chd0);
        if (coluna == (g->c - 1) && i !=0){
            x1+=g->passoX;
            y1=g->y1;
        }
        else
            y1=y2;
    }

}



